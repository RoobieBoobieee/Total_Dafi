Attribute VB_Name = "defaultModule"
Option Private Module
Public Sub Load()
        
    On Error GoTo ErrorHandler:

    'set all default values
    Worksheets("Sheet1").cmbWeeks.Clear
    Worksheets("Sheet1").lstStopsAll.Clear
    Worksheets("Sheet1").lstStopsSelected.Clear
    Worksheets("Sheet1").lblErr.Caption = ""
    Worksheets("Sheet1").txtTitle.Value = ""
    
    Worksheets("Sheet1").txtStopsBeginDate.Value = Date - (Weekday(Date) - 2)
    Worksheets("Sheet1").txtStopsEndDate.Value = Date - (Weekday(Date) - 2) + 4
    
    For i = 1 To DatePart("ww", Date - Weekday(Date, 2) + 4, 2, 2)
        Worksheets("Sheet1").cmbWeeks.AddItem ("Week " & i)
    Next i
    
    Worksheets("Sheet1").cmbWeeks.Value = ("Week " & i - 1)
    
    Dim StrQuery As String
    StrQuery = "SELECT DISTINCT HistStops.[Machine] FROM DAFI.dbo.HistStops HistStops ORDER BY HistStops.Machine;"
   
    'Initializes variables
    Dim cnn As New ADODB.Connection
    Dim rst As New ADODB.Recordset
    Dim ConnectionString As String

    'Setup the connection string for accessing MS SQL database
    ConnectionString = "Provider=SQLOLEDB.1;Password=Arteco01;Persist Security Info=True;User ID=lReportDafi;Data Source=ARTSCHF03\PLANT;Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Use Encryption for Data=False;Tag with column collation when possible=False;Initial Catalog=DAFI"

    'Opens connection to the database
    cnn.Open ConnectionString
    'Timeout error in seconds for executing the entire query; this will run for 15 minutes before VBA timesout, but your database might timeout before this value
    cnn.CommandTimeout = 900

    'Performs the actual query
    rst.Open StrQuery, cnn
    'Dumps all the results from the StrQuery into cell A2 of the first sheet in the active workbook
    rst.MoveFirst
    Do
        Worksheets("Sheet1").lstStopsAll.AddItem (rst![machine])
        rst.MoveNext
    Loop Until rst.EOF
   
Exit Sub  '(this stops the error handler from running when there is no error!)1

ErrorHandler:
    Worksheets("Sheet1").lblErr.Caption = "An error occured -  error  " & Err.Number

     
End Sub

Sub doQuery(query As String)
    'functie voor data sheet te genereren
    'Initializes variables
    Dim cnn As New ADODB.Connection
    Dim rst As New ADODB.Recordset
    Dim ConnectionString As String

    'Setup the connection string for accessing MS SQL database
    ConnectionString = "Provider=SQLOLEDB.1;Password=Arteco01;Persist Security Info=True;User ID=lReportDafi;Data Source=ARTSCHF03\PLANT;Use Procedure for Prepare=1;Auto Translate=True;Packet Size=4096;Use Encryption for Data=False;Tag with column collation when possible=False;Initial Catalog=DAFI"

    'Opens connection to the database
    cnn.Open ConnectionString
    'Timeout error in seconds for executing the entire query; this will run for 15 minutes before VBA timesout, but your database might timeout before this value
    cnn.CommandTimeout = 900

    'Performs the actual query
    rst.Open query, cnn
    'Dumps all the results from the StrQuery into cell A2 of the first sheet in the active workbook
    'Machine Order   Stilstand   Omschrijving    Begin   Einde

    Worksheets("Sheet2").range("A1").Value = "Machine"
    Worksheets("Sheet2").range("B1").Value = "Order"
    Worksheets("Sheet2").range("C1").Value = "Stilstand"
    Worksheets("Sheet2").range("D1").Value = "Omschrijving"
    Worksheets("Sheet2").range("E1").Value = "Begin"
    Worksheets("Sheet2").range("F1").Value = "Einde"
    Worksheets("Sheet2").range("A2").CopyFromRecordset rst

End Sub
