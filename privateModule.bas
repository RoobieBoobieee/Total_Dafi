Attribute VB_Name = "privateModule"
Option Private Module
Sub calcTimeDiff()
        'calculates difference between start and end time
        Worksheets("Sheet2").Columns("G").ColumnWidth = 0
                
        Sheets("Sheet2").Select
        range("G2").Select

        Do Until Selection.Offset(0, -2).Value = ""
                Selection.Value = (Selection.Offset(0, -1).Value - Selection.Offset(0, -2)) * 1440
                Selection.Value = Round(Selection.Value, 0)
                Selection.Offset(1, 0).Select
        Loop

        range("A1").Select

End Sub

Sub fillEmptyOrder()
        
        ' Function that fills empty order cells
        ' Dafi doesnt fill the order cell when the description is "Ombouw"
        
        'only for xlsx files, sql does fill all the orders - not used anymore
        
        Sheets("Sheet2").Select
        range("B2").Select
        
        'loop trough all items
        Do Until Selection.Offset(0, -1).Value = ""
                
                'if a cell is empty and the description is "Ombouw"
                If Selection.Value = "" And Selection.Offset(0, 2) = "Ombouw" Then
                    Dim i As Integer
                    i = 0
                    ' loop until a filled order cell is found as long as there are items (otherwise an infinite loop occurs at the end of the file)  the value of this cell should be the value of the above empty cells
                    Do While Selection.Offset(i, 0).Value = "" And Selection.Offset(i, -1).Value <> ""
                        i = i + 1
                    Loop
                    
                    'only copy orders when machine numbers match - they always do, just to be sure
                    If (Selection.Offset(0, -1).Value = Selection.Offset(i, -1).Value) Then
                        Selection.Value = Selection.Offset(i, 0).Value
                    End If
                End If
                
                Selection.Offset(1, 0).Select
        Loop

        range("A1").Select

End Sub

Sub clearEmptyOrders()
          
        'same as fill empty orders
          
        Sheets("Sheet2").Select
        range("B2").Select

        Do Until Selection.Offset(0, -1).Value = ""
                If Selection.Value = "" Then
                    Selection.EntireRow.Delete
                Else
                    Selection.Offset(1, 0).Select
                End If
        Loop

        range("A1").Select

End Sub

Sub clearSheets()
    Application.DisplayAlerts = False
    For Each ws In Worksheets
        GUI = "Sheet1"
        Sheet = "Sheet2"
        If ws.name <> Sheet And ws.name <> GUI Then ws.Delete
    Next
    Application.DisplayAlerts = True
End Sub

Sub clearCharts()
    Application.DisplayAlerts = False
    For Each Chart In Charts
        Chart.Delete
    Next
    Application.DisplayAlerts = True
End Sub

Sub deleteCharts()
    'deletes charts within workscheet - not used
    Dim chtObj As ChartObject
     For i = 1 To Sheets.Count
        Sheets(Sheets(i).name).Select
        For Each chtObj In ActiveSheet.ChartObjects
            chtObj.Delete
        Next
    Next i
End Sub

Sub calcTimes()
    'turns of screenupdate so the screen doesnt switch everytime the code switches to another worksheet
    Application.ScreenUpdating = False
    
    privateModule.clearSheets
    
    privateModule.calcTimeDiff
    
    Sheets.Add
    ActiveSheet.Move After:=ThisWorkbook.Worksheets(ThisWorkbook.Worksheets.Count)
    ActiveSheet.name = "Sheet3"

    range("A1").Value = "Machine"
    range("B1").Value = "Ordernummer"
    range("C1").Value = "Stilstand"
    range("D1").Value = "Omschrijving"
    range("E1").Value = "Totaaltijd"
    range("F1").Value = "n Keer"
    ActiveSheet.Columns("A:F").AutoFit
    
    Sheets("Sheet2").Select
    range("G2").Select
    
    Dim sheet2, Sheet3 As Integer

    sheet2 = 0
    
    Dim found As Boolean
    
    Dim machine As String, order As String, id As Integer, description As String, time As Integer
    
    Do Until Selection.Value = ""
        
        If Selection.Offset(0, -4).Value <> 6 And Selection.Offset(0, -4).Value <> 7 And Selection.Offset(0, -4).Value <> 98 And Selection.Offset(0, -4).Value <> 99 And Not (Selection.Offset(0, -4).Value >= 994 And Selection.Offset(0, -4).Value <= 997) Then
            
            machine = Selection.Offset(0, -6).Value
            order = Selection.Offset(0, -5).Value
            id = Selection.Offset(0, -4).Value
            description = Trim(Selection.Offset(0, -3).Value)
            time = Selection.Value
            
            If Left(description, 1) = "7" Then
                description = Right(description, Len(description) - 6)
            End If
            
            Sheets("Sheet3").Select
            range("E2").Select
            
            found = False
            
            
            If Worksheets("Sheet1").optLine.Value Or Worksheets("Sheet1").optNLijn.Value Then
                order = "nvt"
            ElseIf Worksheets("Sheet1").optMeh.Value Or Worksheets("Sheet1").optNAantal.Value Then
                machine = "nvt"
                order = "nvt"
            End If
                
            Do Until Selection.Value = ""
                If Selection.Offset(0, -4).Value = machine And Selection.Offset(0, -3).Value = order And Selection.Offset(0, -2).Value = id Then
                    Selection.Value = Selection.Value + time
                    Selection.Offset(0, 1).Value = Selection.Offset(0, 1).Value + 1
                    found = True
                    Exit Do
                End If
                Selection.Offset(1, 0).Select
            Loop
            If Not found Then
                Selection.Offset(0, -4).Value = machine
                Selection.Offset(0, -3).Value = order
                Selection.Offset(0, -2).Value = id
                Selection.Offset(0, -1).Value = description
                Selection.Value = time
                Selection.Offset(0, 1).Value = 1
            End If
        End If
        sheet2 = sheet2 + 1
        
        
        ActiveSheet.Columns("A:E").AutoFit
    
        Sheets("Sheet2").Select
        range("G2").Select
        Selection.Offset(sheet2, 0).Select
    Loop
    
    range("A1").Select

End Sub

Sub calcPerStop()
    'turns of screenupdate so the screen doesnt switch everytime the code switches to another worksheet
    Application.ScreenUpdating = False
    
    privateModule.clearSheets
    
    privateModule.calcTimeDiff
    
    privateModule.clearEmptyOrders
    
    Sheets.Add
    ActiveSheet.Move After:=ThisWorkbook.Worksheets(ThisWorkbook.Worksheets.Count)
    ActiveSheet.name = "Sheet3"

    range("A1").Value = "X"
    range("B1").Value = "X"
    
    Sheets("Sheet2").Select
    range("G2").Select
    
    Dim sheet2, Sheet3 As Integer

    sheet2 = 0
    
    Dim found As Boolean
    
    Dim machine As String, order As String, id As Integer, description As String, time As Integer
    
    Do Until Selection.Value = ""
        
        If Selection.Offset(0, -4).Value <> 6 And Selection.Offset(0, -4).Value <> 7 And Selection.Offset(0, -4).Value <> 98 And Selection.Offset(0, -4).Value <> 99 And Not (Selection.Offset(0, -4).Value >= 994 And Selection.Offset(0, -4).Value <= 997) Then
            
            machine = Selection.Offset(0, -6).Value
            order = Selection.Offset(0, -5).Value
            id = Selection.Offset(0, -4).Value
            description = Trim(Selection.Offset(0, -3).Value)
            time = Selection.Value
            
            If Left(description, 1) = "7" Then
                description = Right(description, Len(description) - 6)
            End If
            
            Sheets("Sheet3").Select
            range("A1").Select
            
            found = False
            Dim index As Integer
            index = 0
            Do Until Selection.Value = ""
                If Selection.Value = description Then
                    Do Until Selection.Value = ""
                        Selection.Offset(1, 0).Select
                    Loop
                    Selection.Value = time
                    Selection.Offset(0, -index + 1).Value = order
                    Selection.Offset(0, -index).Value = machine
                    found = True
                    Exit Do
                End If
                index = index + 1
                Selection.Offset(0, 1).Select
            Loop
            If Not found Then
                Selection.Value = description
                Selection.Offset(1, 0).Value = time
                Selection.Offset(1, -index + 1).Value = order
                Selection.Offset(1, -index).Value = machine
            End If
        End If
        sheet2 = sheet2 + 1
        
        
        ActiveSheet.Columns("A:E").AutoFit
    
        Sheets("Sheet2").Select
        range("G2").Select
        Selection.Offset(sheet2, 0).Select
    Loop
    
    Worksheets("Sheet1").Select
    range("A1").Select

End Sub

Sub drawCharts()

On Error GoTo ErrorHandler:

        
    Application.ScreenUpdating = False
    
    'to be sure no charts remain when drawing new ones
    privateModule.deleteCharts
    
    Sheets("Sheet3").Select
    range("E2").Select
    Dim startcell, endcell As Integer
    startcell = 2
    endcell = 2
    
    Dim xdesc As String, ydesc As String, sourcecolumn As String

    Dim currentvalue As String, nextvalue As String
    
    Do Until Selection.Value = ""
        xdesc = "D" & startcell & ":D" & endcell
        sourcecolumn = "E"
        ydesc = "Tijd (Min)"
        If Worksheets("Sheet1").optLine.Value Or Worksheets("Sheet1").optNLijn.Value Then
            currentvalue = Selection.Offset(0, -4).Value
            nextvalue = Selection.Offset(1, -4).Value
        ElseIf Worksheets("Sheet1").optMeh.Value Or Worksheets("Sheet1").optNAantal.Value Then
            currentvalue = "Chart"
            nextvalue = "Chart"
            If Selection.Offset(1, 0).Value = "" Then
                nextvalue = "TriggerChart"
            End If
        Else
            currentvalue = Selection.Offset(0, -3).Value
            nextvalue = Selection.Offset(1, -3).Value
        End If
        
        If Worksheets("Sheet1").optNOrder.Value Or Worksheets("Sheet1").optNLijn.Value Or Worksheets("Sheet1").optNAantal.Value Then
        sourcecolumn = "F"
        ydesc = "# Voorkomen"
        End If
        
        If currentvalue <> nextvalue Then
            'add conversion chart
            Charts.Add After:=Worksheets(Worksheets.Count)
            With ActiveChart
                .Location Where:=xlLocationAsNewSheet, name:=currentvalue
                .ChartType = xlColumnClustered
                .SetSourceData Source:=Sheets("Sheet3").range(sourcecolumn & startcell & ":" & sourcecolumn & endcell), PlotBy:=xlColumns
                .SeriesCollection(1).XValues = Sheets("Sheet3").range(xdesc)
                .SeriesCollection(1).name = ""
                .SeriesCollection(1).Border.Color = RGB(190, 75, 72)
                .Move After:=Sheets(ActiveWorkbook.Sheets.Count)
                'microsoft fucked up... again... default is false, but setting to true doesnt always work when not first set to false
                .HasTitle = False
                .HasTitle = True
                .ChartTitle.Characters.Text = "Stilstanden  " & currentvalue & " " & Worksheets("Sheet1").txtTitle.Text
                .Axes(xlCategory, xlPrimary).HasTitle = True
                .Axes(xlCategory, xlPrimary).AxisTitle.Characters.Text = "Omschrijving"
                .Axes(xlValue, xlPrimary).HasTitle = True
                .Axes(xlValue, xlPrimary).AxisTitle.Characters.Text = ydesc
            End With
            Sheets("Sheet3").Select
            startcell = endcell + 1
        End If
        endcell = endcell + 1
        Selection.Offset(1, 0).Select
    Loop
    Worksheets("Sheet1").Select
    range("A1").Select
    
    'turn screen update back on
    Application.ScreenUpdating = True

    
    Exit Sub  'this stops the error handler from running when there is no error!

ErrorHandler:
    Worksheets("Sheet1").lblErr.Caption = "An error occured -  error  " & Err.Number

End Sub



